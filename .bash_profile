warn() {
	printf '%s[warning]: %s\n' "$0" "$*"
	continue
}

include() {
	for file
	do
		[ -f "$file" ] || warn "File '$file' is not ordinary file"
		[ -r "$file" ] || warn "File '$file' is not readable"
		. "$file"
	done
}

# include (source) files:
# - .bashrc in login shell (some shells don't do it by default)
# - .profile (to ensure backward compatibility)

include "$HOME/.bashrc" "$HOME/.profile"

# use GNU utilities by default (for Solaris 11 users)
PATH=/usr/gnu/bin:$PATH

# enlarge history size
export HISTFILESIZE=1000000
export HISTSIZE=1000000

# set default pager to less command
export PAGER=less
# set no-case-search as default for less command
export LESS='-i'

