# some ls aliases
alias ll='ls -l'
alias la='ls -A'
alias ls='ls --color=auto'

# human-readable units
alias df='df -h'

# safe file manipulation
alias cp='cp -ip'
alias rm='rm -i'
alias mv='mv -i'

# rev command substitution (for Solaris users), uncomment
#rev="perl -ne 'chomp;print scalar reverse . \"\n\";'"
alias 20='printf "%03d\n" {1..20}'
alias 100='printf "$(echo %4d{,,,,,,,,,})\n" {1..100}'
alias p='stat -Lc "%A [%a] %U/%G %n"'


# quote text (function)
quote () 
{ 
   local quoted=${1//\'/\'\\\'\'};
   printf "'%s'" "$quoted"
}
# get real/effective identity (function)
get_id() {
	"$@" &>/dev/null &
	ps -f -o ruser,ruid,user,uid,rgroup,rgid,group,gid,args -p $!
	kill $! 2>/dev/null
}
