# Programování v shellu

Podpůrné materiály pro seriál [Programování v shellu na YouTube](https://www.youtube.com/playlist?list=PLHS-Uhp7t2Ys5lwy-5jops3t_DCfpXu_j). Většina příkladů si vystačí
s existujícími soubory na běžném UNIX/Linuxovém systému. Někdy je však
praktičtější mít k dispozici soubory s konkrétním obsahem. Tyto soubory jsou
tedy uvedeny v tomto repozitáři.

## Obsah

Obsah repozitáře tvoří především **reálné soubory z existujících systémů**, na
kterých je možné demonstrovat jednotlivé příkazy a jejich chování. Obsah se bude
postupně rozrůstat tak, jak budou vznikat jednotlivé díly seriálu.

Součástí jsou také konfigurační soubory shellu, které si můžete umístit do svého
domovského adresáře, případně jejich části.

* `.bashrc` - konfigurační soubor shellu *bash*, který je načítaný při každém
  spouštění shellu, proto obvykle obsahuje aliasy a deklarace funkcí
* `.bash_profile` - konfigurační soubor shellu *bash*, který je načítaný pouze
  při přihlášení, proto obvykle obsahuje deklarace proměnných a jejich export,
  takže sub-shelly toto nastavení zdědí
* `.inputrc` - konfigurační soubor sheelu *bash*, resp. jeho součásti readline,
  která má se stará o čtení příkazové řádky a o operace na ní
* `apache.log` - log webového serveru Apache HTTPd
* `blocks` - skript, který vypíše soubor po blocích (512B)
* `data.tar` - testovací archiv
* `dos` - ukázka souboru z DOS/Windows, kde řádky končí dvojicí znaků: CR+LF (\r,\n)
* `example1.eml` - Ukázka prostého e-mailu z https://github.com/mikel/mail/tree/master/spec/fixtures
* `example2.eml` - Ukázka e-mailu s přílohou z https://github.com/mikel/mail/tree/master/spec/fixtures
* `grep.1` - zdrojový soubor manuálové stránky příkazu grep
* `group` - reálná databáze skupin uživatelů
* `ls.info` - info stránka příkazu ls
* `mysqlsampledatabase.sql` - dump SQL databáze
* `passwd` - reálná databáze uživatelů obsahující 10.000+ záznamů
* `perms` - skript pro vytvoření adresářové struktury s příklady přístupových práv
* `response` - ukázka HTTP odpovědi
* `slow` - skript, který (náhodně) pomalu vypisuje data ze standardního vstupu
* `sleeep` - skript, který spustí sleep 9999 ignorující signál TERM
* `sendmail` - RC skript pro start/ukončení služby sendmail
* `sshd_config` - konfigurační soubor pro SSH server
* `vh1, vh2` - skripty pro určení vyučovací hodiny
* `vimtutor` - skript pro spouštění výukového režimu VIMu
* `words` - slovník (seznam slov, jedno slovo na řádku), obvykle z adresáře /usr[/share]/dict
